import React from 'react';
import './App.css';
import ContainerComponent from './Components/ContainerComponent/ContainerComponent';

function App() {
  return (
    <div className="App">
      <ContainerComponent></ContainerComponent>
    </div>
  );
}

export default App;
