import React, { Component } from "react";
import "./Container.css";
import jsonData from "../../dataGlasses.json";
import GlassesList from "../GlassesList/GlassesList";

export default class ContainerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { listGlasses: jsonData, choosedGlasses: jsonData[0].url };
  }

  setChoosedGlasses = (id) => {
    this.setState((prev) => {
      return { ...prev, choosedGlasses: jsonData[id].url };
    });
  };

  render() {
    return (
      <div className="container">
        <div className="model">
          <img className="model-img" src="../glassesImage/model.jpg" alt="" />
          <img className="glassesShow" src={this.state.choosedGlasses} alt="" />
        </div>


        {/* Component Glasses */}
        <GlassesList setChoosedGlasses={this.setChoosedGlasses}
          listGlasses={this.state.listGlasses}>
        </GlassesList>
      </div>
    );
  }
}
