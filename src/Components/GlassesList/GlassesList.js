import React, { Component } from 'react';
import GlassesItems from "../GlassesItems/GlassesItems";
import "./GlassesList.css";

export default class GlassesList extends Component {
    render() {
        return (
            <div className="glasses">
                <div id="glasses" className="row row-cols-2 row-cols-md-3 row-cols-lg-6 ">
                    <GlassesItems setChoosedGlasses={this.props.setChoosedGlasses}
                        listGlasses={this.props.listGlasses}
                    ></GlassesItems>
                </div>
            </div>
        )
    }
}
