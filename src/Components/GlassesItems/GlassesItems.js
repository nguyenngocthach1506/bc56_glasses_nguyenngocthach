import React, { Component } from "react";
import "./GlassesItems.css";

export default class GlassesItems extends Component {
  renderGlasses = () => {
    let { listGlasses, setChoosedGlasses } = this.props;

    return listGlasses.map((element, index) => {
      return (
        <button
          className="col"
          key={"glassItem" + index}
          onClick={() => {
            setChoosedGlasses(index);
          }}
          type="button"
        >
          <img
            className="img-glasses"
            src={`./glassesImage/g${index + 1}.jpg`}
            alt=""
          />
        </button>
      );
    });
  };

  render() {
    return <>{this.renderGlasses()}</>;
  }
}
